
from params import *

import reader as rd

from antares import Reader, Treatment, Writer

# ----- Script ----
# Change to True if you want the source base and target base to be printed
check_bases = False

# ------------------
# Reading the files
# ------------------

source_base, target_base = rd.load()

if check_bases:
    print source_base.keys()
    print source_base[0]

    print source_base[0].keys()
    print source_base[0][0]

    print target_base.keys()
    print target_base[0].keys()

# --------------
# Interpolation
# --------------
treatment = Treatment('interpolation')
treatment['source'] = source_base
treatment['target'] = target_base
result = treatment.execute()

print result[0]

# -------------------
# Writing the result
# -------------------
writer = Writer('hdf_antares')
writer['base'] = result
writer['filename'] = output_name
writer.dump()
