from antares import *
import numpy as np

from params import *


def fluct(base, zone, inst, var):
    mean = np.mean(base[:][:][var])
    fluctuation = base[zone][inst][var] - mean
    return fluctuation
    

def print_base(base):
    print '\n\n\n'
    print '---------'
    print base
    print base[0]
    print base[0].keys()
    print '---------'
    print '\n\n\n'
    return None


def load():
    verbose(3)

    # ------------------
    # Reading Mesh 1
    # ------------------
    r=Reader('hdf_avbp')
    r['filename']=mesh_in_path
    r['shared']=True
    source_base_mesh=r.read()


#   print_base(source_base_mesh)

    # ------------------
    # Reading Solution 1
    # ------------------
    r2 = Reader('hdf_avbp')
    r2['base'] = source_base_mesh
    r2['filename'] = sol_in_path
    source_base = r2.read()

#   print_base(source_base)

    # ------------------
    # Reading Mesh 2
    # ------------------
    r3=Reader('hdf_avbp')
    r3['filename']=mesh_out_path
   #r2['shared']=True
    target_base=r3.read()

    print_base(target_base)

    return source_base, target_base


if __name__ == "__main__":
    # --------------
    # Interpolation
    # --------------
    treatment = Treatment('interpolation')
    treatment['source'] = load_meshin()
    treatment['target'] = load_meshout()
    result = treatment.execute()

    print_base(result)

#   # -------------------
#   # Writing the result
#   # -------------------
#   #writer = Writer('bin_tp')
#   writer = Writer('hdf_antares')
#   writer['base'] = result
#   writer['filename'] = 'ex_interpolation.plt'
#   writer.dump()

